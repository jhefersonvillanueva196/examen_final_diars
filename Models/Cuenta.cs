﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_DIARS.Models
{
    public class Cuenta
    {
        public int id { get; set; }
        public int id_user { get; set; }
        public string nombre { get; set; }
        public string categoria { get; set; }
        public int sInicial { get; set; }
    }
}
