﻿using Final_DIARS.BDD;
using Final_DIARS.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Controllers
{
    public class FinanzasController : Controller
    {
        private FinanzasContext context;
        
        public FinanzasController(FinanzasContext context)
        {
            this.context = context;
        }

        public List<Cuenta> getCuentas()
        {
            var cu = context.cuentas;
            var id = GetLoggedUser().id;
            var cuenta = cu.Where(r => r.id_user == id).ToList();
            return cuenta;
        }
        public List<Gasto> getGastos(int id)
        {
            var ga = context.gastos;
            var gasto = ga.Where(r => r.id_cuenta == id).ToList();
            return gasto;
        }

        public List<Ingreso> getIngresos(int id)
        {
            var ing = context.ingresos;
            var ingreso = ing.Where(r => r.id_cuenta == id).ToList();
            return ingreso;
        }

        public List<Solicitud> getSolicitudes(int id)
        {
            var sol = context.solicitudes;
            var solic = sol.Where(r => r.id_uDestino == id).ToList();
            return solic;
        }

        public List<Contacto> getContactos(int id)
        {
            var con = context.contactos;
            var contac = con.Where(r => r.id_user == id).ToList();
            return contac;
        }

        public Cuenta buscaCuenta(string nombre)
        {
            var cu = context.cuentas;
            var nom = nombre;
            Cuenta cuenta = cu.First(r => r.nombre == nom);
            return cuenta;
        }
        public Cuenta buscaCuentaUser(string nombre, string destino)
        {
            var cu = context.cuentas;
            var nom = destino;
            var id = getUsuario(nombre).id;
            Cuenta cuenta = cu.First(r => r.nombre == nom && r.id_user==id);
            return cuenta;
        }

        public Usuario getUsuario(string nombre)
        {
            var u = context.usuarios;
            var user = u.First(r => r.nombre == nombre);
            return user;
        }

        public Usuario getUsuarioId(int id)
        {
            var u = context.usuarios;
            var user = u.First(r => r.id == id);
            return user;
        }

        public Solicitud buscaSolicitud(int id)
        {
            var s = context.solicitudes;
            var sol = s.First(r => r.id == id);
            return sol;
        }


        [HttpGet]
        public IActionResult MostrarCuentas()
        {
            var liCuentas = getCuentas();
            return View("MostrarCuentas",liCuentas);
        }
        [HttpGet]
        public IActionResult CrearCuenta()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CrearCuentas(string nombre,string tp,int saldo)
        {
            var id = GetLoggedUser().id;
            Cuenta cu = new Cuenta
            {
                id_user = id,
                nombre = nombre,
                categoria = tp,
                sInicial = saldo
            };
            context.cuentas.Add(cu);
            context.SaveChanges();
            var liCuentas = getCuentas();
            return View("MostrarCuentas",liCuentas);
        }

        [HttpGet]
        public IActionResult AgregarGastos()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AgregarGasto(string nombre, DateTime fecha, string descripcion, int saldo)
        {
            Cuenta cuenta = buscaCuenta(nombre);
            var diferencia = cuenta.sInicial - saldo;
            if (diferencia < 0)
            {
                TempData["AuthMessage"] = "Saldo insuficiente";
                return RedirectToAction("AgregarGastos");
            }
            else
            {
                cuenta.sInicial = cuenta.sInicial - saldo;
                Gasto gasto = new Gasto
                {
                    id_cuenta = cuenta.id,
                    cuenta = nombre,
                    descripcion = descripcion,
                    fecha = fecha,
                    monto = saldo,
                };
                context.cuentas.Update(cuenta);
                context.gastos.Add(gasto);
                context.SaveChanges();
                var liCuentas = getCuentas();
                return RedirectToAction("MostrarCuentas", liCuentas);
            }
        }

        [HttpGet]
        public IActionResult AgregarIngresos()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AgregarIngreso(string nombre, DateTime fecha,string descripcion,int saldo)
        {
            Cuenta cuenta = buscaCuenta(nombre);
            cuenta.sInicial = cuenta.sInicial + saldo;
            Ingreso ingreso = new Ingreso
            {
                id_cuenta = cuenta.id,
                cuenta = nombre,
                descripcion = descripcion,
                fecha = fecha,
                monto = saldo,
            };
            context.cuentas.Update(cuenta);
            context.ingresos.Add(ingreso);
            context.SaveChanges();
            var liCuentas = getCuentas();
            return RedirectToAction("MostrarCuentas", liCuentas);
        }

        [HttpGet]
        public IActionResult Transferencias()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Transferencia(string origen, string destino, DateTime fecha, int saldo)
        {
            Cuenta cor = buscaCuenta(origen);
            Cuenta des = buscaCuenta(destino);
            var diferencia = cor.sInicial - saldo;
            if (diferencia < 0)
            {
                TempData["AuthMessage"] = "Saldo insuficiente";
                return RedirectToAction("Trasferencia");
            }
            else
            {
                cor.sInicial = cor.sInicial - saldo;
                des.sInicial = des.sInicial + saldo;
                Gasto gasto = new Gasto
                {
                    id_cuenta = cor.id,
                    cuenta = origen,
                    descripcion = "Transferencia",
                    fecha = fecha,
                    monto = saldo,
                };
                Ingreso ingreso = new Ingreso
                {
                    id_cuenta = des.id,
                    cuenta = destino,
                    descripcion = "Transferencia",
                    fecha = fecha,
                    monto = saldo,
                };
                context.cuentas.Update(cor);
                context.cuentas.Update(des);
                context.ingresos.Add(ingreso);
                context.gastos.Add(gasto);
                context.SaveChanges();
                var liCuentas = getCuentas();
                return RedirectToAction("MostrarCuentas", liCuentas);
            }
        }

        [HttpGet]
        public IActionResult Gastos(int id)
        {
            var liGastos = getGastos(id);
            return View("Gastos", liGastos);
        }
        [HttpGet]
        public IActionResult Ingresos(int id)
        {
            var liIngresos = getIngresos(id);
            return View("Ingresos", liIngresos);
        }
        [HttpGet]
        public IActionResult EnviarSolicitudes()
        {
            return View();
        }
        [HttpPost]
        public IActionResult EnviarSolicitud(string nombre)
        {
            var id = GetLoggedUser().id;
            var user = getUsuario(nombre).id;
            var nom = GetLoggedUser().nombre;
            Solicitud solicitud = new Solicitud
            {
                id_uOrigen = id,
                id_uDestino = user,
                nombre = nom
            };
            context.solicitudes.Add(solicitud);
            context.SaveChanges();
            var liCuentas = getCuentas();
            return View("MostrarCuentas", liCuentas);
        }

        [HttpGet]
        public IActionResult MostrarSolicitudes()
        {
            var id = GetLoggedUser().id;
            var liSolicitud = getSolicitudes(id);
            return View("MostrarSolicitudes",liSolicitud);
        }

        public IActionResult AceptarSolicitud(int id)
        {
            var id_user = GetLoggedUser().id;
            var nom = buscaSolicitud(id).nombre;
            var sol = buscaSolicitud(id);
            Contacto contacto = new Contacto
            {
                id_user = id_user,
                nombre = nom
            };
            context.contactos.Add(contacto);
            context.solicitudes.Remove(sol);
            context.SaveChanges();
            var liCuentas = getCuentas();
            return View("MostrarCuentas", liCuentas);
        }

        [HttpGet]
        public IActionResult MostrarContactos()
        {
            var id = GetLoggedUser().id;
            var liContacto = getContactos(id);
            return View("MostrarContactos",liContacto);
        }

        [HttpGet]
        public IActionResult TransferenciaUsuario()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TransferenciaUsuarios(string origen, string nombre, string destino, DateTime fecha, int saldo)
        {
            Cuenta cor = buscaCuenta(origen);
            Cuenta des = buscaCuentaUser(nombre,destino);
            var diferencia = cor.sInicial - saldo;
            if (diferencia < 0)
            {
                TempData["AuthMessage"] = "Saldo insuficiente";
                return RedirectToAction("Trasferencia");
            }
            else
            {
                cor.sInicial = cor.sInicial - saldo;
                des.sInicial = des.sInicial + saldo;
                Gasto gasto = new Gasto
                {
                    id_cuenta = cor.id,
                    cuenta = origen,
                    descripcion = "Transferencia",
                    fecha = fecha,
                    monto = saldo,
                };
                Ingreso ingreso = new Ingreso
                {
                    id_cuenta = des.id,
                    cuenta = destino,
                    descripcion = "Transferencia",
                    fecha = fecha,
                    monto = saldo,
                };
                context.cuentas.Update(cor);
                context.cuentas.Update(des);
                context.ingresos.Add(ingreso);
                context.gastos.Add(gasto);
                context.SaveChanges();
                var liCuentas = getCuentas();
                return RedirectToAction("MostrarCuentas", liCuentas);
            }
        }

        private Usuario GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.usuarios.First(o => o.nombre == username);
            return user;
        }
    }
}
