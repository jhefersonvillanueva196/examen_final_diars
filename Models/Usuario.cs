﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_DIARS.Models
{
    public class Usuario
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string contrasenia { get; set; }
    }
}
