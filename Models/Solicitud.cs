﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_DIARS.Models
{
    public class Solicitud
    {
        public int id { get; set; }
        public int id_uOrigen { get; set; }
        public int id_uDestino { get; set; }
        public string nombre { get; set; }
    }
}
