﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_DIARS.Models
{
    public class Gasto
    {
        public int id { get; set; }
        public int id_cuenta { get; set; }
        public string cuenta { get; set; }
        public DateTime fecha { get; set; }
        public string descripcion { get; set; }
        public int monto { get; set; }
    }
}
