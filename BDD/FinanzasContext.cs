﻿using Final_DIARS.BDD.Mapping;
using Final_DIARS.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_DIARS.BDD
{
    public class FinanzasContext:DbContext
    {
        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<Cuenta> cuentas { get; set; }
        public DbSet<Gasto> gastos { get; set; }
        public DbSet<Ingreso> ingresos { get; set; }
        public DbSet<Contacto> contactos { get; set; }
        public DbSet<Solicitud> solicitudes { get; set; }

        public FinanzasContext(DbContextOptions<FinanzasContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new CuentaMap());
            modelBuilder.ApplyConfiguration(new GastoMap());
            modelBuilder.ApplyConfiguration(new IngresoMap());
            modelBuilder.ApplyConfiguration(new ContactoMap());
            modelBuilder.ApplyConfiguration(new SolicitudMap());
        }
    }
}
