﻿using Final_DIARS.BDD;
using Final_DIARS.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Examen_Final_DIARS.Controllers
{
    public class AuthController : Controller
    {
        private FinanzasContext context;

        private IConfiguration configuration;

        public AuthController(FinanzasContext context, IConfiguration config)
        {
            this.context = context;
            this.configuration = config;
        }
        /**/
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var user = context.usuarios
                .FirstOrDefault(o => o.nombre == username && o.contrasenia == CreateHash(password));
            if (user == null)
            {
                TempData["AuthMessage"] = "Usuario o contraseña incorrecto";
                return RedirectToAction("Login");
            }
            //Autenticación del usuario
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.nombre),
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            HttpContext.SignInAsync(claimsPrincipal);

            return RedirectToAction("MostrarCuentas", "Finanzas");
        }
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }

        public string Create(string password)
        {
            return CreateHash(password);
        }

        private string CreateHash(string input)
        {
            input += configuration.GetValue<string>("Key");
            var sha = SHA512.Create();

            var bytes = Encoding.Default.GetBytes(input);
            var hash = sha.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }
        /**/
        [HttpGet]
        public IActionResult CrearUsuario()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CrearUsuario(string username, string password)
        {
            string pass = CreateHash(password);
            // no hay mensajes => 0 mensaje
            Usuario user = new Usuario
            {
                nombre = username,
                contrasenia = pass
            };

            context.usuarios.Add(user);
            context.SaveChanges();
            return RedirectToAction("Index","Home");
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
